#!/bin/bash
set -e

sync() {
  local srcdir="$1"
  local dstdir="$2"
  cp -r "$srcdir"/* "$dstdir" | true
}

monitor() {
  local srcdir="$1"
  local dstdir="$2"
  inotifywait -m -r -e create -e modify -e delete "$srcdir" | while read EVENT
  do
    sync "$srcdir" "$dstdir"
  done
}

srcdir='../devfiles'
dstdir='examples'
sync "$srcdir" "$dstdir"
monitor "$srcdir" "$dstdir" &
exec npm start
