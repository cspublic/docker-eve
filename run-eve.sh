#!/bin/bash

# Run Eve in docker container
#
# Supply a directory as argument to copy files in the directory into
# Eve "examples" whenever they change

set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
DKR_IMAGE='codesimple/eve'


build_docker () {
    docker build --tag "$DKR_IMAGE" "$SCRIPT_DIR/docker_ctx"
}


run_docker () {
    local eve_file="$(readlink -f "$1")"
    local vols
    if [[ -z "$eve_file" ]]; then
        vols='';
    else 
        vols="-v $eve_file:/home/eve/devfiles:ro";
    fi

    docker run -it --rm -p 8080:8080 $vols "$DKR_IMAGE"
}


build_docker
run_docker "$1"

